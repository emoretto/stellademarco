﻿<? include "db.php" ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Instituto Stella Demarco</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="css/bootstrap.css" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="css/mobile.css" />

<script type="text/javascript" async="async" src="http://www.google-analytics.com/ga.js"></script>

<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>

<!-- MENU -->


<div id="topo" > 
   <div class="container">
     <a href="index.mobile.html" id="home"><img src="img/home_m.png"></a>     
  </div>
</div>


  <nav class="navbar navbar-default" role="navigation">
  <!-- Brand and toggle get grouped for better mobile display -->
  <div class="navbar-header">
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
      <span class="sr-only">Toggle navigation</span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
      <span class="icon-bar"></span>
    </button>
    <a class="navbar-brand" href="#">Menu</a>
  </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
  <div class="collapse navbar-collapse navbar-ex1-collapse">
    
    <ul class="nav navbar-nav">
     <li><a id="firstMenu" href="sobre_mob.html">SOBRE O ISD</a></li>
    <li><a href="projetos_mob.html">PROJETOS</a></li>
    <li><a href="depoimentos_mob.html">DEPOIMENTOS</a></li>
    <li><a href="parceiros_mob.html">PARCEIROS</a></li>
    <li><a href="doacoes_mob.html">DOAÇÕES</a></li>
    <li><a href="livros_mob.php">LIVROS</a></li>
    <li><a id="lastMenu" href="contato_mob.html">CONTATO</a></li>
       
    </ul>
    
  
  </div><!-- /.navbar-collapse -->
</nav>


<!-- END MENU -->

<div class="container centro" id="main">


  <div id="seta"></div>

  <!-- ROW -->

  <div class="row">
  <br/>
   <div class="col-md-8 col-xs-10 titulo">

  <p><span>Biblioteca “Livros Que Curam” <img src="img/seta.png"></span></p>
  </div>
	<br/>
  
 		<div class="col-md-8 col-xs-10 " style="">

            <form role="form" action="livros.php" method="POST" enctype="multipart/form-data">
             <div class="form-group">
                <label for="query">Busca</label>
                <input type="text" maxlength="20" style="width:30%; display: inline; height: 24px;" class="form-control" id="query" name="query" />
                <button type="submit" style="height: 24px; padding-top: 2px;" class="btn btn-primary">Buscar</button>
              </div>
             
               <div class="form-group">
               
              </div>
            </form>


            <?php

            $db = new MyDB();
            $sql = "SELECT * FROM LIVRO ORDER BY ID DESC";

            if(isset( $_REQUEST['query']))
              $sql = "SELECT * FROM LIVRO where titulo like '%".$_REQUEST['query']."%' OR resumo like '%".$_REQUEST['query']."%' or autor like '%".$_REQUEST['query']."%' ORDER BY ID DESC";

            $ret = $db->query($sql);

            while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
            ?>
                <div style="float:left; ">
                  <img width="110px" src="uploads/<?=$row['CAPA']?>"/>
                </div>

                <div style="margin-left: 120px;">
                  <? if ( $row['LINK'] != null ){ ?>
                    <p class="livro_titulo"><a href="<?=$row['LINK']?>" target="_blank"><?=$row['TITULO']?></a></p>
                  <? }else{ ?>
                    <p class="livro_titulo"><a href="uploads/<?=$row['ARQUIVO']?>"><?=$row['TITULO']?></a></p>
                  <? } ?>                  
                  <p class="link_titulo"><?=$row['AUTOR']?></p>
                  <p><?=$row['RESUMO']?></p>                
                </div>
                <span class="hr col-md-10"></span>
                <br/>                
            <?
            }             
            $db->close();
            ?>
	        <br/><br/>
	     </div>

      

  </div> <!-- ROW -->
  </div> <!-- container -->




     <footer>
          <div class="container">
          <div class="row">
            
            <div id="apoio" class="col-md-6 col-xs-6" style="min-width:230px; margin-bottom: 30px;">
            <p>Apoio</p>
        <li><a href="http://instagram.com/cookiequeenbr" target="_blank">Cookie Queen</a></li>
             <li><a href="http://www.crisalbuquerquefotografias.com/" target="_blank">Cris Albuquerque Fotografias</a></li>
            <li><a href="http://www.cucadesign.com/" target="_blank">Cuca Design</a></li>
            <li><a href="http://www.gehealthcare.com.br" target="_blank">GE Healthcare</a></li>        
            <li><a href="http://mareines-patalano.com.br/"  target="_blank">Mareines+Patalano</a></li>
            <li><a href="http://www.nextar.biz/" target="_blank">Nextar</a></li>
            <li><a href="http://www.sequencia.com.br/" target="_blank">Seqüência Cinematográfica</a></li>     
        </div>

        <div class="col-md-4 col-xs-6" style="min-width:230px; margin-bottom: 30px;">
            <p>Mantenedor</p>
            <p><a href="http://www.orbium.com.br/" target="_blank"><img width="180" src="img/logo-orbium.png"/></a></p>
        </div>

          </div>
          </div>
        </footer>



<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

</body>
</html>

