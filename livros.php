﻿<?php include "db.php" ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Instituto Stella Demarco</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="css/bootstrap.css" rel="stylesheet"/>
<link href="css/css.css" rel="stylesheet"/>

<script type="text/javascript" async="async" src="http://www.google-analytics.com/ga.js"></script>

<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>

<!-- MENU -->
<div id="topo" > 
   <div class="container">
     <a href="index.html" id="home"></a>
      <ul class="nav nav-tabs" style="">    
        <li><a id="firstMenu" href="sobre.html">SOBRE O ISD</a></li>
        <li ><a href="projetos.html">PROJETOS</a></li>
        <li><a href="depoimentos.html">DEPOIMENTOS</a></li>
        <li><a href="parceiros.html">PARCEIROS</a></li>
        <li><a href="doacoes.html">DOAÇÕES</a></li>
        <li class="active"><a href="livros.php">LIVROS</a></li>
        <li><a id="lastMenu" href="contato.html">CONTATO</a></li>
      </ul>
  </div>
</div>
<!-- END MENU -->

<div class="container" id="main">

	<div id="topbox">
	  <div class="row" id="topboxin">

	    <div id="carrosel" class="carousel ">
	        <img width="600" height="250" class="w600" src="img/capacontato.jpg" alt="capa"/>
	    </div>
	    
      <div class="col-md-3" id="topboxtext">
        <h2 class="text-center">Projeto "Livros Que Curam"</h2>      
      </div>
	              
	  </div>
	</div>

	<!-- ROW -->
  <div class="titulo">
    <br/>
    <p><span>Biblioteca “Livros Que Curam” <img src="img/seta.png"></span></p>
  </div>


  Aqui você poder ler e imprimir gratuitamente o livro que você precisa.
	<br/>
  <br/>
  
  <div class="row">
	
 		<div class="col-md-8 col-xs-10 " style="">

            <form role="form" action="livros.php" method="POST" enctype="multipart/form-data">
             <div class="form-group">
                <label for="query">Busca</label>
                <input type="text" maxlength="20" style="width:30%; display: inline; height: 24px;" class="form-control" id="query" name="query" />
                <button type="submit" style="height: 24px; padding-top: 2px;" class="btn btn-primary">Buscar</button>
              </div>
            </form>

            <?php

            $db = new MyDB();
            $sql = "SELECT * FROM LIVRO ORDER BY ID DESC";

            if(isset( $_REQUEST['query'])){
              $sql = "SELECT * FROM LIVRO where titulo like '%".$_REQUEST['query']."%' OR resumo like '%".$_REQUEST['query']."%' or autor like '%".$_REQUEST['query']."%' ORDER BY ID DESC";
            }

            $ret = $db->query($sql);

            while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
            ?>
                <div style="float:left; ">
                  <img width="110px" src="uploads/<?=$row['CAPA']?>"/>
                </div>

                <div style="margin-left: 120px;">
                  <? if ( $row['LINK'] != null ){ ?>
                    <p class="livro_titulo"><a href="<?=$row['LINK']?>" target="_blank"><?=$row['TITULO']?></a></p>
                  <? }else{ ?>
                    <p class="livro_titulo"><a href="uploads/<?=$row['ARQUIVO']?>"><?=$row['TITULO']?></a></p>
                  <? } ?>
                  <p class="link_titulo"><?=$row['AUTOR']?></p>
                  <p><?=$row['RESUMO']?></p>                
                </div>
                <span class="hr col-md-10"></span>
                <br/>                
            <?php
            }             
            $db->close();
            ?>
	        <br/><br/>
	    </div>

	</div> <!-- ROW -->
	

</div> <!-- container -->

<footer>

<div class="container">
<br/>
    <div id="apoio" class="col-md-4 col-xs-4">
        <p>Apoio</p>
        <li><a href="http://www.crisalbuquerquefotografias.com/" target="_blank">Cris Albuquerque Fotografias</a></li>
        <li><a href="http://www.cucadesign.com/" target="_blank">Cuca Design</a></li>
        <li><a href="http://www.gehealthcare.com.br" target="_blank">GE Healthcare</a></li>        
        <li><a href="http://mareines-patalano.com.br/"  target="_blank">Mareines+Patalano</a></li>
        <li><a href="http://www.nextar.biz/" target="_blank">Nextar</a></li>
        <li><a href="http://www.sequencia.com.br/" target="_blank">Seqüência Cinematográfica</a></li>
    </div>

    <div class="col-md-4 col-xs-4">
        <p>Mantenedor</p>
        <p><a href="http://www.orbium.com.br/" target="_blank"><img width="240" src="img/logo-orbium.png"/></a></p>
      </div>

    <div class="col-md-4 col-xs-4">
        <span class="hidden-xs hidden-sm">Junte-se a nós</span>
        <p></p>
        <div class="row">        
          <div class="col-md-2 col-xs-2"><a href="https://www.facebook.com/institutostellademarco" target="_blank"><img src="img/facebook.png"/></a></div>
          <div class="col-md-2 col-xs-2"><img src="img/linkedin.png"/></div>
        </div>
        <p></p>
        <p>São Paulo, SP, Brasil</p>
        <p>(11) 2127-1806</p>
        <p>contato@isdemarco.org.br</p>
     </div>
</div>

</footer>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

</body>
</html>