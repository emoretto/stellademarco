﻿<?php 

session_start();

if( !isset($_SESSION['login']) ) {
  echo "Não autorizado";
  return;
}

?> 

<? include "db.php" ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<title>Instituto Stella Demarco</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="css/bootstrap.css" rel="stylesheet"/>
<link href="css/css.css" rel="stylesheet"/>

<script type="text/javascript" async="async" src="http://www.google-analytics.com/ga.js"></script>

<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body

<!-- MENU -->
<div id="topo" > 
   <div class="container">
     <a href="index.html" id="home"></a>
  </div>
</div>
<!-- END MENU -->

<div class="container" id="main">

<h3>Gerenciador de Livros</h3>
<br/>
	<!-- ROW -->

	
	<div class="row">
	
 		<div class="col-md-8 col-xs-10 " style="">
	
            <form role="form" action="upload.php" method="POST" enctype="multipart/form-data">
             <div class="form-group">
                <label for="name">Título</label>
                <input type="text" maxlength="100" data-validation-required-message="O nome é obrigatório" class="form-control" id="titulo" name="titulo" placeholder="Título do livro" required/>
              </div>
              <div class="form-group">
                <label for="autor">Autor</label>
                <input name="autor" type="text" class="form-control" id="autor" placeholder="" required/>
                <p class="help-block"></p>
              </div>
              <div class="form-group">
                <label for="telephone1">Resumo</label>
                <textarea name="resumo" type="text" class="form-control" id="resumo" placeholder="" required></textarea>
              </div>

              <div class="form-group">
                <label for="capa">Capa do livro (apenas .jpg e 120px de largura)</label>
                <input name="capa" type="file" class="form-control" id="capa" name="capa" placeholder="" required/>
                <p class="help-block"></p>
              </div>

               <div class="form-group">
                <label for="arquivo">Arquivo do livro (apenas .pdf)</label>
                <input name="arquivo" type="file" class="form-control" id="arquivo" name="arquivo" placeholder=""/>
                <p class="help-block"></p>
              </div>

              <div class="form-group">
                <label for="link">Link para o livro</label>
                <input name="link" type="text" class="form-control" id="link" placeholder="é preciso iniciar com http://"/>
                <p class="help-block"></p>
              </div>

               <div class="form-group">
               <button type="submit" class="btn btn-primary btn-lg">Enviar</button>
              </div>
            </form>



            <br/>
            <br/>
            <h3>Livros cadastrados:</h3>
            <?php

            $db = new MyDB();
            $sql = "SELECT * FROM LIVRO ORDER BY ID DESC";
            $ret = $db->query($sql);

            while($row = $ret->fetchArray(SQLITE3_ASSOC) ){
            ?>
                <span style="float:right"><a onclick="return confirm('Certeza?');" href="remover_livro.php?id=<?=$row['ID']?>">Remover livro</a></span>      
                <p class="livro_titulo"><a href="<?=$row['ARQUIVO']?>"><?=$row['TITULO']?></a></p>
                <p class="link_titulo"><?=$row['AUTOR']?></p>
                <p><?=$row['RESUMO']?></p>       

                <span class="hr col-md-8"></span>
                <br/>                
            <?
            }             
            $db->close();
            ?>
	        <br/><br/>
	    </div>

		<div class="col-md-4 col-xs-6 titulo">	     
		
		</div>

	</div> <!-- ROW -->
	

</div> <!-- container -->

<footer>

<div class="container">
<br/>


</div>

</footer>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

</body>
</html>

