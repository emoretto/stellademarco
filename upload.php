﻿<?php

include "db.php";

$target_dir = "uploads/";

$target_arquivo = "";

if( $_FILES["arquivo"]["name"] != null ){
	$target_arquivo = $target_dir . basename( $_FILES["arquivo"]["name"] );

	if (move_uploaded_file($_FILES["arquivo"]["tmp_name"], $target_arquivo)) {
	    echo "Arquivo ". basename( $_FILES["arquivo"]["name"]). " enviado <br/>";
	} else {
	    echo "Não foi possível mover o arquivo (livro PDF) no servidor.<br/>";
	}
}

$target_capa = $target_dir . basename( $_FILES["capa"]["name"]);

if (move_uploaded_file($_FILES["capa"]["tmp_name"], $target_capa)) {
    echo "Arquivo ". basename( $_FILES["capa"]["name"]). " enviado<br/>";
} else {
    echo "Não foi possível mover o arquivo (capa do livro) no servidor.<br/>";
}


$db = new MyDB();
$db->adicionaLivro($_REQUEST['titulo'],$_REQUEST['resumo'],$_REQUEST['autor'],$_REQUEST['link'], $_FILES["arquivo"]["name"],$_FILES["capa"]["name"]);
$db->close();
?>



<br/>
<br/>
<a href="novo_livro.php">Voltar</a>