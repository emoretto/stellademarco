
<?php

require 'class.phpmailer.php';

$mail = new PHPMailer;

$mail->isSMTP();                                     // Set mailer to use SMTP 

//$mail->SMTPDebug  = 2;  
$mail->Timeout = 30;
$mail->Port       = 587; 
$mail->Host = 'smtp.nexxy.com.br';  // Specify main and backup server
$mail->SMTPAuth = true;                               // Enable SMTP authentication
$mail->Username = 'enviaisd';                            // SMTP username
$mail->Password = 'envia.123';                           // SMTP password
$mail->From = 'enviaisd@isdemarco.org.br';
$mail->FromName = 'Site ISD';
$mail->addAddress('recebe@isdemarco.org.br');               // Name is optional

$mail->WordWrap = 50;                                 // Set word wrap to 50 characters
$mail->isHTML(true);                                  // Set email format to HTML

$mail->Subject = 'Cadastro no site ISD';
$mail->Body    = 'Nome: '.$_REQUEST['nome'].'<br/>E-mail: '.$_REQUEST['email'].'<br/>IP: '.$_SERVER['REMOTE_ADDR'];

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
<script type="text/javascript" src="js/detectmobilebrowser.js"></script>

<title>Instituto Stella Demarco</title>
<meta http-equiv="Content-Type" content="text/html;charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
<link href="css/bootstrap.css" rel="stylesheet"/>
<link href="css/css.css" rel="stylesheet"/>

<script type="text/javascript"  async="async" src="http://www.google-analytics.com/ga.js"></script>


<!--[if lt IE 9]>
  <script src="js/html5shiv.js"></script>
  <script src="js/respond.min.js"></script>
<![endif]-->

</head>

<body>

<!-- CONTAINER -->


<!-- MENU -->
<div id="topo" > 
   <div class="container">
     <a href="index.html" id="home"></a>
      <ul class="nav nav-tabs" style="">    
        <li><a id="firstMenu" href="sobre.html">SOBRE O ISD</a></li>
        <li><a href="projetos.html">PROJETOS</a></li>
        <li><a href="depoimentos.html">DEPOIMENTOS</a></li>
        <li><a href="parceiros.html">PARCEIROS</a></li>
        <li><a href="doacoes.html">DOAÇÕES</a></li>
        <li><a id="lastMenu" href="contato.html">CONTATO</a></li>
      </ul>
  </div>
</div>
<!-- END MENU -->


<div class="container" id="main" style="height:400px; padding:50px">


<?php
if(!$mail->send()) {
   echo 'Sua mensagem não pode ser enviada.';
  // //echo 'Mailer Error: ' . $mail->ErrorInfo;
   exit;
}else{ ?>

	<div class="col-md-4 col-xs-6 titulo">
       <p><span>Mensagem enviada, obrigado!	</span></p></div>
<?php
}
?>



</div> <!--  CONTAINER -->


<footer>
<div class="container">
<br/>
  <div id="apoio" class="col-md-4 col-xs-4">
        <p>Apoio</p>
         <li><a href="http://www.crisalbuquerquefotografias.com/" target="_blank">Cris Albuquerque Fotografias</a></li>
        <li><a href="http://www.cucadesign.com/" target="_blank">Cuca Design</a></li>
        <li><a href="http://www.gehealthcare.com.br" target="_blank">GE Healthcare</a></li>        
        <li><a href="http://mareines-patalano.com.br/"  target="_blank">Mareines+Patalano</a></li>
        <li><a href="http://www.nextar.biz/" target="_blank">Nextar</a></li>
        <li><a href="http://www.sequencia.com.br/" target="_blank">Seqüência Cinematográfica</a></li>     
    </div>

    <div class="col-md-4 col-xs-4">
        <p>Mantenedor</p>
        <p><a href="http://www.orbium.com.br/" target="_blank"><img width="240" src="img/logo-orbium.png"/></a></p>
      </div>

    <div class="col-md-4 col-xs-4">
        <span class="hidden-xs hidden-sm">Junte-se a nós</span>
        <p></p>
        <div class="row">        
          <div class="col-md-2 col-xs-2"><img src="img/facebook.png"/></div>
          <div class="col-md-2 col-xs-2"><img src="img/twitter.png"/></div>
          <div class="col-md-2 col-xs-2"><img src="img/linkedin.png"/></div>
        </div>
        <p></p>
        <p>São Paulo, SP, Brasil</p>
        <p>(11) 2127-1806</p>
        <p>contato@isdemarco.org.br</p>
     </div>
</div>

</footer>

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>

</body>

</html>


